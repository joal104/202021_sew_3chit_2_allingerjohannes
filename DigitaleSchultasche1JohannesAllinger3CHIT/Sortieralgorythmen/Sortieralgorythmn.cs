﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BubbleSort___Sortieralgorythmen_10._09._2020_ALLINGER_Johannes_3CHIT
{
    class Sortieralgorythmn
    {
        public void BubbelSort(int[] a)
        {
            int t;
            for (int p = 0; p <= a.Length - 2; p++)
            {
                for (int i = 0; i <= a.Length - 2; i++)
                {
                    if (a[i] > a[i + 1])
                    {
                        t = a[i + 1];
                        a[i + 1] = a[i];
                        a[i] = t;
                    }
                }
            }
        }

        public void Quick_Sort(int[] a, int left, int right)
        {
            if (left < right)
            {
                int pivot = Partition(a, left, right);

                if (pivot > 1)
                {
                    Quick_Sort(a, left, pivot - 1);
                }
                if (pivot + 1 < right)
                {
                    Quick_Sort(a, pivot + 1, right);
                }
            }

        }

        public static int Partition(int[] a, int left, int right)
        {
            int pivot = a[left];
            while (true)
            {

                while (a[left] < pivot)
                {
                    left++;
                }

                while (a[right] > pivot)
                {
                    right--;
                }

                if (left < right)
                {
                    if (a[left] == a[right]) return right;

                    int temp = a[left];
                    a[left] = a[right];
                    a[right] = temp;

                }
                else
                {
                    return right;
                }
            }
        }

        public List<int> Merge_Sort(List<int> unsorted)
        {
            if (unsorted.Count <= 1)
                return unsorted;

            List<int> left = new List<int>();
            List<int> right = new List<int>();

            int middle = unsorted.Count / 2;
            for (int i = 0; i < middle; i++)  //Dividing the unsorted list
            {
                left.Add(unsorted[i]);
            }
            for (int i = middle; i < unsorted.Count; i++)
            {
                right.Add(unsorted[i]);
            }

            left = Merge_Sort(left);
            right = Merge_Sort(right);
            return Merge(left, right);
        }

        private static List<int> Merge(List<int> left, List<int> right)
        {
            List<int> result = new List<int>();

            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First() <= right.First())  //Comparing First two elements to see which is smaller
                    {
                        result.Add(left.First());
                        left.Remove(left.First());      //Rest of the list minus the first element
                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                    }
                }

                else if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                }
                else if (right.Count > 0)
                {
                    result.Add(right.First());

                    right.Remove(right.First());
                }
            }
            return result;
        }

        public int[] Merge_Sort3(int[]daten)
        {
            if (daten.Length <= 1)
                return daten;
            
            int middle = daten.Length / 2;

            int[] left = new int[middle];
            int[] right = new int[daten.Length - middle];

            for (int i = 0; i < middle; i++)  //Dividing the unsorted list
            {
                left[i] = daten[i];
            }
            for (int i = middle; i < middle; i++)
            {
                right[i-middle] = daten[i];
            }



            left = Merge_Sort3(left);
            right = Merge_Sort3(right);

            //for(int i = 0; i < daten.Length; i++)
            //{
            //    Console.Write(i + " ");
            //}

            return Merge3(left, right);

            
        }

        private int[] Merge3(int[] left, int[] right)
        {
            int[] result = new int[left.Length + right.Length];

            int r = 0;
            int l = 0;
            int pos = 0;

            while (left.Length > l || right.Length > r)
            {
                if (left.Length > l && right.Length > r)
                {
                    if (left[l] <= right[r])  //Comparing First two elements to see which is smaller
                    {

                        result[pos] = left[l];
                        l++;   //Rest of the list minus the first element
                        pos++;
                    }
                    else
                    {
                        result[pos] = right[r];
                        r++;
                        pos++;
                    }
                }

                else if (left.Length > l)
                {
                    result[pos] = left[l];
                    l++;
                    pos++;
                }
                else if (right.Length > r)
                {
                    result[pos] = right[r];
                    r++;
                    pos++;
                }
            }
            return result;
        }

        public void SelectionSort(int[] daten)
        {
            
            
            int smallest;
            for (int i = 0; i < daten.Length - 1; i++)
            {
                smallest = i;

                for (int index = i + 1; index < daten.Length; index++)
                {
                    if (daten[index] < daten[smallest])
                    {
                        smallest = index;
                    }
                }
                Swap(daten, i, smallest);
               
            }

            
        }

        public void Swap(int[] daten, int first, int second)
        {
            int temporary = daten[first];
            daten[first] = daten[second];
            daten[second] = temporary;
        }

    }
}

