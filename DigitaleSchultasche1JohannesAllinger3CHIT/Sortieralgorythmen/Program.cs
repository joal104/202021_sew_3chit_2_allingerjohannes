﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace BubbleSort___Sortieralgorythmen_10._09._2020_ALLINGER_Johannes_3CHIT
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] daten = new int [1000];
            List<int> unsorted = new List<int>();
            NumberRandom(daten);
            NumberRandom2(unsorted);




            //Stopwatch sw1 = new Stopwatch();
            //sw1.Start();
            //new Sortieralgorythmn().BubbelSort(daten);
            //sw1.Stop();

            //Console.WriteLine("BubbleSort: " + sw1.Elapsed);


            //Stopwatch sw2 = new Stopwatch();
            //sw2.Start();
            //new Sortieralgorythmn().Quick_Sort(daten, 0, daten.Length - 1);
            //sw2.Stop();

            //Console.WriteLine("QuickSort: " + sw2.Elapsed);

            //List<int> unsorted = new List<int>();

            //NumberRandom2(unsorted);


            //Stopwatch sw3 = new Stopwatch();
            //sw3.Start();
            //new Sortieralgorythmn().Merge_Sort(unsorted);
            //sw3.Stop();

            //Console.WriteLine("MergeSort mit list: " + sw3.Elapsed);

            //Stopwatch sw4 = new Stopwatch();
            //sw4.Start();
            //new Sortieralgorythmn().Merge_Sort3(daten);
            //sw4.Stop();

            //Console.WriteLine(sw4.Elapsed);

            Stopwatch sw5 = new Stopwatch();
            sw5.Start();
            new Sortieralgorythmn().SelectionSort(daten);
            sw5.Stop();

            Console.WriteLine(sw5.Elapsed);






        }

        static void NumberRandom(int[] daten)
        {
            Random r = new Random();

            for(int i = 0; i < daten.Length-1; i++)
            {
                daten[i] = r.Next(0,100);
            }
        }

        static void NumberRandom2(List<int> unsorted)
        {
            Random r1 = new Random();

            for (int i = 0; i < 10; i++)
            {
                unsorted.Add(r1.Next(0, 100));
               
            }

        }
    }
}
