﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Dynamic;

namespace Tierheim_Datenbank_Logik
{
    public class CRUD
    {
        public List<Tier> Tiere = new List<Tier>();

        public void Load()
        {
            Tiere.Clear();
            using (StreamReader sr = new StreamReader("Tierheim_Datenbank_ALLINGER_Johannes.txt"))
            {
                while (sr.Peek() != -1)
                {
                    string[] help = sr.ReadLine().Split(Convert.ToChar(";"));

                    switch (help[0])
                    {
                        case "Hund":
                            Tiere.Add(new Hund(help[1], DateTime.ParseExact(help[2], "dd.MM.yyyy", null)));
                            break;
                        case "Vogel":
                            Tiere.Add(new Vogel(Convert.ToInt32(help[1]), DateTime.ParseExact(help[2], "dd.MM.yyyy", null)));
                            break;
                    }


                }
            }

            
        }

        public void create()
        {
            using (StreamWriter sw = new StreamWriter("Tierheim_Datenbank_ALLINGER_Johannes.txt", false))
            {
                for (int i = 0; i < Tiere.Count; i++)
                {
                    if (Tiere[i].GetType().ToString().Substring(25) == "Hund")
                    {
                        sw.Write("Hund" + ";" + ((Hund)Tiere[i]).bark() + ";" + ((Hund)Tiere[i]).date() + ";" + "\n");
                    }
                    else if (Tiere[i].GetType().ToString().Substring(25) == "Vogel")
                    {
                        sw.Write("Vogel" + ";" + ((Vogel)Tiere[i]).date() + ";" + ((Vogel)Tiere[i]).Flügel() + ";" + "\n");
                    }
                }
            }
        }

        public void remove(int idnumber)
        {
            if(idnumber <= Tiere.Count)
            {
                Tiere.RemoveAt(idnumber - 1);
                create();
            }
        }


    }
}
