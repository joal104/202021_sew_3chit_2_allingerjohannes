﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tierheim_Datenbank_Logik
{
    public class Hund : Tier
    {
        protected string bellen;
        
        public Hund(string bellen, DateTime aufnahmedatum)
        {
            this.bellen = bellen;
            this.aufnahmedatum = aufnahmedatum;
        }

        public string bark()
        {
            return bellen;
        }

        public string date()
        {
            return aufnahmedatum.ToString("dd.MM.yyyy");
        }

        public override string ToString()
        {
            return "Hund:" + " " + bellen + " " + aufnahmedatum.ToString("dd.MM.yyyy");
        }
    }
}
