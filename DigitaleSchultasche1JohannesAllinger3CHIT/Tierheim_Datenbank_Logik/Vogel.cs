﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tierheim_Datenbank_Logik
{
    public class Vogel : Tier
    {
        public int Flügelspannweite;
        public Vogel(int Flügelspannweite, DateTime aufnahmedatum)
        {
            this.Flügelspannweite = Flügelspannweite;
            this.aufnahmedatum = aufnahmedatum;
        }

        public int Flügel()
        {
            return Flügelspannweite;
        }

        public string date()
        {
            return aufnahmedatum.ToString("dd.MM.yyyy");
        }
        public override string ToString()
        {
            return "Vogel:" + " " + Flügelspannweite + " " + aufnahmedatum;
        }
    }
}
