﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Vogel : Tier
    {
        int Flügelspannweite;
        public Vogel(int Flügelspannweite, DateTime aufnahmedatum)
        {
            this.Flügelspannweite = Flügelspannweite;
            this.aufnahmedatum = aufnahmedatum;
        }

        public override string ToString()
        {
            return "Vogel:" + " " + Flügelspannweite + " " + aufnahmedatum;
        }
    }
}
