﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Hund : Tier
    {
        string bellen;
        public Hund(string bellen, DateTime aufnahmedatum)
        {
            this.bellen = bellen;
            this.aufnahmedatum = aufnahmedatum;
        }

        public override string ToString()
        {
            return "Hund:" + " " + bellen + " " + aufnahmedatum;
        }
    }
}
