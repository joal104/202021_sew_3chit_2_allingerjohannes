﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Tierheim_Datenbank_Logik;

namespace Tierheim_Datenbank_CMD_ALLINGER_Johannes
{
    class Program
    {
        static CRUD cr = new CRUD();
        static string type;
        static string datum;
        static string attribute;
        static void Main(string[] args)
        {
            
            cr.Load();
            
            
            

            while (true)
            {
                Console.WriteLine("Datenbank Tierheim Zwettl");
                
                string eingabe1 = Console.ReadLine();

                switch(eingabe1)
                {
                    case "Hund":
                        read("Hund");
                        break;
                    case "Vogel":
                        read("Vogel");
                        break;
                    case "add":
                        add("add");
                        break;
                    case "remove":
                        löschen();
                        break;
                }

                


                



            }
           


        }
        static void read(string eingabe)
        {
            if (eingabe == "Hund")
            {
                for (int i = 0; i < cr.Tiere.Count; i++)
                {
                    if (cr.Tiere[i].GetType().ToString().Substring(25) == "Hund")
                    {
                        Console.WriteLine(cr.Tiere[i]);
                    }
                }
            }

            if (eingabe == "Vogel")
            {
                for (int i = 0; i < cr.Tiere.Count; i++)
                {
                    if (cr.Tiere[i].GetType().ToString().Substring(25) == "Vogel")
                    {
                        Console.WriteLine(cr.Tiere[i]);
                    }
                }
            }
        }

        static void add(string eingabe2)
        {
            

            if (eingabe2 == "add")
            {
                Console.Write("Tier: ");
                type = Console.ReadLine();


                Console.Write("Attribut: ");
                attribute = Console.ReadLine();

                Console.Write("Datum: ");
                datum = Console.ReadLine();

                bool prüfen = false;

               while(prüfen == false)
               {
                    try
                    {
                        if(DateTime.Parse(datum) <= DateTime.Now)
                        {
                            prüfen = true;
                        }

                        else
                        {
                            Console.Write("Datum: ");
                            datum = Console.ReadLine();
                        }

                    }
                    catch
                    {
                        Console.Write("Datum: ");
                        datum = Console.ReadLine();
                    }
               }
                

                

            }

            if(type == "Hund")
            {
                cr.Tiere.Add(new Hund(attribute, Convert.ToDateTime(datum)));
            }
            else if(type == "Vogel")
            {
                cr.Tiere.Add(new Vogel(Convert.ToInt32(attribute), Convert.ToDateTime(datum)));
            }
                


            cr.create();
        }

        static void löschen()
        {
            string id;
            id = Console.ReadLine();

            cr.remove(Convert.ToInt32(id));

        }

    }
}
