﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woerterbuch_CMD
{
    class Program
    {
        static Dictionary Words1 = new Dictionary();
        static void Main(string[] args)
        {
            //myDict.Add("Schule", "school");
            //myDict.Add("Haus", "house");
            //myDict.Add("Maus", "mouse");

            //Console.WriteLine(myDict["Schule"]);
            //Console.WriteLine(DE["Schuhe"]); //liefert Exception
            //if (myDict.ContainsKey("Schuhe"))
            //    Console.WriteLine(myDict["Schuhe"]);


            Words1.Read();

            bool cmd = true;
            while (cmd == true)
            {
                Console.Write("#");
                string Input = Console.ReadLine();

                //if (Input.Contains("delete "))
                //{
                //    string inputstring = Input.Substring(7);
                //    if (inputstring != "")
                //    {
                //        Words1.Delete(inputstring);
                //    }
                //}
                //if (Input.Contains("translate "))
                //{
                //    string inputstring = Input.Substring(10);
                //    if (inputstring != "")
                //    {
                //        Console.WriteLine(Words1.Translate(inputstring));
                //    }
                //}

                switch (Input)
                {
                    case "list":
                        Output(); break;
                    case "reload":
                        Words1.Read(); break;
                    case "clear":
                        Console.Clear(); break;
                    case "new word":
                        Add(); break;
                    case "test":
                        learn(); break;
                    case "exit":
                        cmd = false; break;
                    case "delete":
                        delete(); break;
                    default:
                        break;
                }
            }
            //if (Words1.myDict.ContainsKey("Schule"))
            //    Console.WriteLine(myDict["Schule"]);
        }
        static void Output()
        {
            foreach (var item in Words1)
            {
                Console.WriteLine($"{item.Key}/{item.Value}");
            }
        }
        static void Add()
        {
            Console.Write("Deutsch: ");
            string DE = Console.ReadLine();

            Console.Write("Englisch: ");
            string EN = Console.ReadLine();

            Console.Write(Words1.Add(DE, EN));
        }

        static void delete()
        {
            Console.Write("Deutsch: ");
            string DE1 = Console.ReadLine();
            
            Console.Write("Englisch: ");
            string EN1 = Console.ReadLine();

            Words1.Delete(DE1, EN1);
        }
        static void learn()
        {
            Console.Clear();
            int ran = Words1.RandomWord();

            Console.Write(Words1.ElementAt(ran).Key + " = ");
            string input = Console.ReadLine();

            if (Words1.ElementAt(ran).Value == input)
            {
                Console.WriteLine("true");
                learn();
            }
                
            else
            {
                Console.Write("false -> ");
                Console.WriteLine(Words1.ElementAt(ran).Value);
            }
        }
    }
}
