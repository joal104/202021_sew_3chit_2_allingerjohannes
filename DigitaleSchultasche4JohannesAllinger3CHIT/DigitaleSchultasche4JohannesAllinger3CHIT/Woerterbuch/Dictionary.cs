﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woerterbuch_CMD
{
    public class Dictionary : Dictionary<string,string>
    {
        public void Delete(string inputstring1, string inputstring2)
        {
            foreach (var item in this)
            {
                if (item.Key.ToString() == inputstring1)
                {
                    this.Remove(item.Key);
                    break;
                }
                else if(item.Value.ToString() == inputstring2)
                {
                    this.Remove(item.Value);
                    break;
                }
            }
            Write();
        }
        public void Read() //aus dem Textdokumet Woerter auslesen
        {
            base.Clear();
            using (StreamReader sr = new StreamReader("words.txt"))
            {
                while (sr.Peek() != -1)
                {
                    string file_content = sr.ReadLine();
                    string[] words = file_content.Split(';');

                    base.Add(words[0].ToString(), words[1].ToString());
                }
            }
        }
        public void Write() //in das Textdokument Woerter eintragen
        {
            using (StreamWriter sw = new StreamWriter("words.txt",false))
            {
                foreach (var item in this)
                {
                    sw.WriteLine($"{item.Key};{item.Value};");
                }
            }
            Read();
        }
        public new string Add(string DE,string EN)
        {
            if (this.ContainsKey(DE))
            {
                if (this[DE] == EN)
                    return null;

                else
                    return "\nDoppelter Eintrag";
            }
            else
            {
                base.Add(DE, EN);
                Write();
                return null;
            }


        }
        public string Translate(string inputstring)
        {
            foreach (var item in this)
            {
                if (inputstring == item.Key.ToString())
                {
                    return item.Value;
                }
            }
            return "Kein Wort dazu gefunden.";
        }
        public int RandomWord()
        {
            Random r = new Random();

            return r.Next(0, this.Count());
        }
        public new string this[string word]
        {
            get
            {
                if (this.ContainsKey(word))
                    return base[word];

                return null;
            }
        }

    }
}
