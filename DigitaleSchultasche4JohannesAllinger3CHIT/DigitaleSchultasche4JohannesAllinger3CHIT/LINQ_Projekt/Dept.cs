﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScottLINQ
{
    class Dept
    {
        public int deptno { get; set; }
        public string dname { get; set; }
        public string loc { get; set; }
        public Dept(int deptno = 0, string dname = "(new)", string loc = "(unassigned)")
        {
            this.deptno = deptno;
            this.dname = dname;
            this.loc = loc;
        }
    }
}
