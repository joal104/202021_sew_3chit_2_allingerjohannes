﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScottLINQ
{
    class Scott_Schema
    {
        public List<Emp> emp = new List<Emp>(){
            new Emp(7369, "SMITH", "CLERK", 7902, new DateTime(1980, 12, 17), 800, null, 20),
            new Emp(7499, "ALLEN", "SALESMAN", 7698, new DateTime(1981, 2, 20), 1600, 11, 30),
            new Emp(7521, "WARD", "SALESMAN", 7698, new DateTime(1981, 2, 22), 1250, 22, 30),
            new Emp(7566, "JONES", "MANAGER", 7839, new DateTime(1981, 04, 02), 2975, 55, 20),
            new Emp(7654, "MARTIN", "SALESMAN", 7698, new DateTime(1981, 9, 28), 1250, null, 30),
            new Emp(7698, "BLAKE", "MANAGER", 7839, new DateTime(1981, 5, 1), 2850, null, 30),
            new Emp(7782, "CLARK", "MANAGER", 7839, new DateTime(1981, 9, 6), 2450, null, 10),
            new Emp(7788, "SCOTT", "ANALYST", 7566, new DateTime(1982, 12, 9), 3000, null, 20),
            new Emp(7839, "KING", "PRESIDENT", null, new DateTime(1981, 11, 17), 5000, 44, 10),
            new Emp(7844, "TURNER", "SALESMAN", 7689, new DateTime(1981, 9, 8), 1500, null, 30),
            new Emp(7876, "ADAMS", "CLERK", 7788, new DateTime(1983, 1, 12), 1100, null, 20),
            new Emp(7900, "JAMES", "CLERK", 7698, new DateTime(1981, 12, 3), 950, 31, 30),
            new Emp(7902, "FORD", "ANALYST", 7566, new DateTime(1981, 12, 3), 3000, null, 20),
            new Emp(7934, "MILLER", "CLERK", 7782, new DateTime(1982, 1, 23), 1300, null, 10)
        };

        public List<Dept> dept = new List<Dept>(){
            new Dept(10, "ACCOUNTING", "NEW YORK"), 
            new Dept(20, "RESEARCH", "DALLAS"), 
            new Dept(30, "SALES", "CHICAGO"), 
            new Dept(40, "OPERATIONS", "BOSTON")
        };

        public List<int> dummy = new List<int>() { 1 };
    }
}
