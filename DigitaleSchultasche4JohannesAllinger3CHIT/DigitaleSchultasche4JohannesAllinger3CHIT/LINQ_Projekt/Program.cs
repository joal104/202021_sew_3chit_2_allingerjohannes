﻿using System;
using System.Linq;
using ScottLINQ;

namespace LINQ_Projekt
{
    class Program
    {
        static void Main(string[] args)
        {
            Scott_Schema scott = new Scott_Schema();

            //var erg1 = from d in scott.dept
            //           where d.deptno >= 20
            //           select d; //select *

            //foreach (var item in erg1)
            //{
            //    Console.WriteLine($"{item.dname} {item.loc}");
            //}

            //Console.WriteLine("-----------------");

            //var erg2 = from d in scott.dept
            //           where d.deptno >= 20
            //           select new { abteilung = d.dname, ort = d.loc };

            //foreach (var item in erg2)
            //{
            //    Console.WriteLine($"{item.abteilung} {item.ort}");
            //}

            //foreach (var item in erg2)
            //{
            //    Console.WriteLine(item);
            //}

            //Uebung 1
            //var erg3 = from d in scott.dept
            //           select d;

            //foreach (var item in erg3)
            //{
            //    Console.WriteLine($"{item.dname} {item.loc}");
            //}

            //Console.WriteLine("--------------");

            ////Uebung2
            //var erg4 = from emp in scott.emp
            //           where emp.deptno == 10
            //           select emp;

            //foreach (var item in erg4)
            //{
            //    Console.WriteLine($"{item.ename} {item.job} {item.hiredate}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung3
            //var erg5 = from emp in scott.emp
            //           select emp;

            //foreach (var item in erg5)
            //{
            //    Console.WriteLine($"{item.ename} {item.job} {item.sal}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung4
            //var erg6 = from emp in scott.emp
            //           where emp.deptno != 10
            //           select emp;

            //foreach (var item in erg6)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung5
            //var erg7 = from emp in scott.emp
            //           where emp.sal < emp.comm
            //           select emp;

            //foreach (var item in erg7)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung6
            //var erg8 = from emp in scott.emp
            //           where emp.hiredate == new DateTime(1981, 12, 03)
            //           select emp;

            //foreach (var item in erg8)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung7
            //Console.WriteLine("Uebung7");
            //var erg9 = from emp in scott.emp
            //           where emp.sal < 1250 || emp.sal > 1600
            //           select new { emp.ename, emp.sal };

            //foreach (var item in erg9)
            //{
            //    Console.WriteLine($"{item.ename} {item.sal}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung8
            //var erg10 = from emp in scott.emp
            //            where emp.job != "manager" && emp.job != "president"
            //            select emp;

            //foreach (var item in erg10)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung9
            //var erg11 = from emp in scott.emp
            //            where emp.ename[2] == 'A'
            //            select emp;

            //foreach (var item in erg11)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung10

            //var erg12 = from emp in scott.emp
            //            where emp.comm != null
            //            select new { emp.empno, emp.ename, emp.job };

            //foreach (var item in erg12)
            //{
            //    Console.WriteLine($"{item.ename} {item.job} {item.empno}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung11

            //var erg13 = from emp in scott.emp
            //            orderby emp.comm
            //            select emp;

            //foreach (var item in erg13)
            //{
            //    Console.WriteLine($"{item.ename} / {item.comm}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung12

            //var erg14 = from emp in scott.emp
            //            where emp.job != "MANAGER" && emp.job != "PRESIDENT"
            //            orderby emp.deptno, emp.hiredate descending
            //            select emp;

            //foreach (var item in erg14)
            //{
            //    Console.WriteLine($"{item.ename} / {item.deptno} / {item.hiredate}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung13

            //var erg15 = from emp in scott.emp
            //          where emp.ename.Length == 6
            //          select new { emp.ename };

            //foreach (var item in erg15)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung14

            //var erg16 = from d in scott.emp
            //            where d.deptno == 30
            //            select new { name = d.ename, beruf = d.job };

            //foreach (var item in erg16)
            //{
            //    Console.WriteLine($"{item.name} - {item.beruf}");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung16

            //var erg17 = from d in scott.emp
            //          select new { name = d.ename, MONTHLY = d.sal, DAILY = (d.sal / 22), HOURLY = (d.sal / 22 / 8) };

            //foreach (var item in erg17)
            //{
            //    Console.WriteLine($"{ item.name } - { item.MONTHLY } - { item.DAILY } - { item.HOURLY }");
            //}

            //Console.WriteLine("-----------------------");

            ////Uebung17

            //var erg18 = (from d in scott.emp
            //           select d.sal).Sum();

            //Console.WriteLine($"{erg18}");


            //Uebung19

            var sal = (from emp in scott.emp
                       where emp.comm != null && emp.deptno == 30
                       select emp.deptno).Count();

            Console.WriteLine("Gehalt: "+sal);

            var comm = (from emp in scott.emp
                        where emp.sal != null && emp.deptno == 30
                        select emp.deptno).Count();

            Console.WriteLine("Provision: "+comm);

            Console.WriteLine("---------------------------------");


            //Uebung20

            var jobs = (from emp in scott.emp
                        group emp by emp.job into j
                        select j).Count();

            Console.WriteLine("Anzahl der einzelnen Jobs: "+jobs);

            Console.WriteLine("---------------------------------");


            //Uebung22

            //var ue22 = (from emp in scott.emp
            //           where emp.deptno == 30
            //           group emp by emp.deptno into j
            //           select (Summe: )
            //Console.WriteLine(scott.emp.Sum(j => j.deptno));

            

            //Uebung23

            var jobs2 = (from emp in scott.emp
                         where emp.job != "MANAGER" && emp.job != "PRESIDENT"
                        group emp by emp.job into j
                        select j).Count();

            Console.WriteLine(jobs2);

            Console.WriteLine("----------------------------------------");

            //Uebung25

            var ue25 = from emp in scott.emp
                       where emp.job == "MANAGER" || emp.job == "PRESIDENT"  
                       select emp;
                       

            foreach(var item in ue25)
            {
                Console.WriteLine($"{item.ename}");
            }

            //Uebung26

            Console.WriteLine("-------------------------------");

            var ue26 = from emp in scott.emp
                       where ((emp.sal / 100) * 25) < emp.comm
                       select emp;

            foreach (var item in ue26)
            {
                Console.WriteLine($"{item.ename}, {item.job}, {item.comm}");
            }



            Console.ReadKey();
        }
    }
}
