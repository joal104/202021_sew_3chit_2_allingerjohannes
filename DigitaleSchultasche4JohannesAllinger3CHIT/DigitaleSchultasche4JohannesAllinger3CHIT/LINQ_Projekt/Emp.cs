﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScottLINQ
{
    class Emp
    {
        public int empno { get; set; }
        public string ename { get; set; }
        public string job { get; set; }
        public int? mgr { get; set; }
        public DateTime? hiredate { get; set; }
        public int sal { get; set; }
        public int? comm { get; set; }
        public int deptno { get; set; }

        public Emp(int empno=0, string ename="(new)", string job="(unassigned)",int? mgr=null, 
                   DateTime? hiredate=null, int sal=0, int? comm=null, int deptno=0)
        {
            this.empno = empno;
            this.ename = ename;
            this.job = job;
            this.mgr = mgr;
            this.hiredate=hiredate;
            this.sal = sal;
            this.comm = comm;
            this.deptno = deptno;
        }
    }
}
