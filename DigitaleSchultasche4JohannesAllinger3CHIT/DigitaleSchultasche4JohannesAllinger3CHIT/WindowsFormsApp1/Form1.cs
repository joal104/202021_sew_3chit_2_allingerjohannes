﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woerterbuch_CMD;

namespace Woerterbch_Forms
{
    public partial class Form1 : Form
    {
        Dictionary Words1 = new Dictionary();
        public int r;
        public Form1()
        {
            InitializeComponent();
        }

        private void Add(object sender, EventArgs e)
        {
            string DE = Deutsch_Box.Text;
            string EN = Englisch_Box.Text;
            if (!DE.Contains(":") && !EN.Contains(":"))
            {
                Words1.Add(DE, EN);
            }

        }

        private void Delete(object sender, EventArgs e)
        {
            string DE1 = Deutsch_Box.Text;
            string EN1 = Englisch_Box.Text;
            Words1.Delete(DE1, EN1);
        }

        private void list(object sender, EventArgs e)
        {
            Output(sender, e);
        }

        private void learning(object sender, EventArgs e)
        {
            Deutsch_Box.Text = "";
            Englisch_Box.Text = "";


            Deutsch_Box.Text = Words1.ElementAt(r).Key;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string input = Englisch_Box.Text;

            if (Words1.ElementAt(r).Value == input)
            {
                TrueFalse.Text = ("True");
            }
            else
            {
                TrueFalse.Text = ("False" + Words1.ElementAt(r).Value);
            }
        }

        private void random()
        {
            r = Words1.RandomWord();
            Deutsch_Box.Text = Words1.ElementAt(r).Key;
        }

        //private void DE_Box_MouseClick(object sender, MouseEventArgs e)
        //{
        //    Deutsch_Box.Clear();
        //}
        //private void EN_Box_MouseClick(object sender, MouseEventArgs e)
        //{
        //    Englisch_Box.Clear();
        //}
        private void Output(object sender, EventArgs e)
        {
            Words1.Read();
            listBox1.Items.Clear();
            foreach (var item in Words1)
            {
                listBox1.Items.Add($"{item.Key}/{item.Value}");
            }
        }

        private void Englisch_Box_TextChanged(object sender, EventArgs e)
        {

        }


        //private void delete(object sender, EventArgs e)
        //{
        //    Words1.Delete(Words1.ElementAt(listBox1.SelectedIndex).Key);
        //}
    }
}
