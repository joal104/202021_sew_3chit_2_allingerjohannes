﻿namespace Woerterbch_Forms
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Deutsch_Box = new System.Windows.Forms.TextBox();
            this.Englisch_Box = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.TrueFalse = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(38, 49);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(442, 264);
            this.listBox1.TabIndex = 0;
            // 
            // Deutsch_Box
            // 
            this.Deutsch_Box.Location = new System.Drawing.Point(538, 49);
            this.Deutsch_Box.Name = "Deutsch_Box";
            this.Deutsch_Box.Size = new System.Drawing.Size(176, 20);
            this.Deutsch_Box.TabIndex = 1;
            this.Deutsch_Box.Tag = "";
            // 
            // Englisch_Box
            // 
            this.Englisch_Box.Location = new System.Drawing.Point(538, 127);
            this.Englisch_Box.Name = "Englisch_Box";
            this.Englisch_Box.Size = new System.Drawing.Size(176, 20);
            this.Englisch_Box.TabIndex = 2;
            this.Englisch_Box.TextChanged += new System.EventHandler(this.Englisch_Box_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(57, 358);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 47);
            this.button1.TabIndex = 3;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Add);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(206, 358);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 47);
            this.button2.TabIndex = 4;
            this.button2.Text = "Learn";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.learning);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(347, 358);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(97, 47);
            this.button3.TabIndex = 5;
            this.button3.Text = "List";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.list);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(538, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(47, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "Deutsch";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(538, 106);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(47, 20);
            this.textBox2.TabIndex = 7;
            this.textBox2.Text = "Englisch";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(472, 358);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(97, 47);
            this.button4.TabIndex = 8;
            this.button4.Text = "Delete";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Delete);
            // 
            // TrueFalse
            // 
            this.TrueFalse.Location = new System.Drawing.Point(538, 241);
            this.TrueFalse.Name = "TrueFalse";
            this.TrueFalse.Size = new System.Drawing.Size(100, 20);
            this.TrueFalse.TabIndex = 9;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(538, 174);
            this.button5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 21);
            this.button5.TabIndex = 10;
            this.button5.Text = "Check";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(758, 450);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.TrueFalse);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Englisch_Box);
            this.Controls.Add(this.Deutsch_Box);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox Deutsch_Box;
        private System.Windows.Forms.TextBox Englisch_Box;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox TrueFalse;
        private System.Windows.Forms.Button button5;
    }
}

