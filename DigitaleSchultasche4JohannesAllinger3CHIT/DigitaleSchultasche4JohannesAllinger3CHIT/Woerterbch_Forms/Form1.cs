﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woerterbuch_CMD;
using System.Threading;

namespace Woerterbch_Forms
{
    public partial class Form1 : Form
    {
        Dictionary Words1 = new Dictionary();
        public int r;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DeutschTextBox.Visible = false;
            EnglischTextBox.Visible = false;
            textBox3.Visible = false;
            textBox2.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;

            delete2.Visible = false;
            add1.Visible = false;
            start.Visible = false;
            check.Visible = false;
            list2.Visible = false;
            listBox1.Visible = false;

            pictureBox3.Visible = false;
            pictureBox4.Visible = false;

            truefalse.Visible = false;
            
            

        }

        private void add_Click(object sender, EventArgs e)
        {
            add.Visible = false;
            delete.Visible = false;
            list.Visible = false;
            learn.Visible = false;

            DeutschTextBox.Visible = true;
            EnglischTextBox.Visible = true;
            textBox3.Visible = true;
            textBox2.Visible = true;
            add1.Visible = true;

        }

        private void delete_Click(object sender, EventArgs e)
        {
            add.Visible = false;
            delete.Visible = false;
            list.Visible = false;
            learn.Visible = false;

            DeutschTextBox.Visible = true;
            EnglischTextBox.Visible = true;
            textBox3.Visible = true;
            textBox2.Visible = true;
            delete2.Visible = true;
        }

        private void learn_Click(object sender, EventArgs e)
        {
            add.Visible = false;
            delete.Visible = false;
            list.Visible = false;
            learn.Visible = false;
            DeutschTextBox.Visible = false;
            EnglischTextBox.Visible = false;
            textBox3.Visible = false;
            textBox2.Visible = false;
            delete2.Visible = false;
            add1.Visible = false;

            start.Visible = true;
            check.Visible = true;

            textBox4.Visible = true;
            textBox5.Visible = true;

            truefalse.Visible = true;

        }

        private void list_Click(object sender, EventArgs e)
        {
            add.Visible = false;
            delete.Visible = false;
            list.Visible = false;
            learn.Visible = false;

            list2.Visible = true;
            listBox1.Visible = true;

        }

        private void Output(object sender, EventArgs e)
        {
            Words1.Read();
            listBox1.Items.Clear();
            foreach (var item in Words1)
            {
                listBox1.Items.Add($"{item.Key};{item.Value};");
            }
        }

        private void add1_Click(object sender, EventArgs e)
        {
            string DE = DeutschTextBox.Text;
            string EN = EnglischTextBox.Text;
            if (!DE.Contains(":") && !EN.Contains(":"))
            {
                Words1.Add(DE, EN);
            }

            DeutschTextBox.Text = "";
            EnglischTextBox.Text = "";
        }

        private void start_Click(object sender, EventArgs e)        
        {
            
            random();
            textBox4.Text = "";
            textBox5.Text = "";


            textBox4.Text = Words1.ElementAt(r).Key;
        }

        private void check_Click(object sender, EventArgs e)
        {
            
            string input = textBox5.Text;
            

            if (Words1.ElementAt(r).Value == input)
            {
                truefalse.ForeColor = Color.LightGreen;
                truefalse.Text = ("True");
                pictureBox3.Visible = true;
                pictureBox4.Visible = false;



                start_Click(sender, e);
            }
            else
            {
                
                truefalse.ForeColor = Color.Red;
                truefalse.Text = ("False" + " " + Words1.ElementAt(r).Value);
                
                pictureBox4.Visible = true;
                pictureBox3.Visible = false;

                start_Click(sender, e);

            }
        }

        private void random()
        {
            r = Words1.RandomWord();
            textBox4.Text = Words1.ElementAt(r).Key;
        }

        private void list2_Click(object sender, EventArgs e)
        {
            Output(sender, e);
        }

        private void exit_Click(object sender, EventArgs e)
        {
            DeutschTextBox.Visible = false;
            EnglischTextBox.Visible = false;
            textBox3.Visible = false;
            textBox2.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;

            delete2.Visible = false;
            add1.Visible = false;
            start.Visible = false;
            check.Visible = false;
            list2.Visible = false;
            listBox1.Visible = false;

            add.Visible = true;
            delete.Visible = true;
            learn.Visible = true;
            list.Visible = true;

            pictureBox4.Visible = false;
            pictureBox3.Visible = false;
            truefalse.Visible = false;
        }

        private void delete2_Click(object sender, EventArgs e)
        {
            string DE1 = DeutschTextBox.Text;
            string EN1 = EnglischTextBox.Text;
            Words1.Delete(DE1, EN1);

            DeutschTextBox.Text = "";
            EnglischTextBox.Text = "";
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
