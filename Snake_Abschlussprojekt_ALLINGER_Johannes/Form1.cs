﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake_Abschlussprojekt_ALLINGER_Johannes
{
    public partial class Form1 : Form
    {
        Controller cl = new Controller();
        

        Timer timer = new Timer();

        Graphics g;



        public Form1()
        {
            InitializeComponent();
            
        }


        

        public void Form1_Load(object sender, EventArgs e)
        {
            CenterToScreen();

            //this.DoubleBuffered = true;

            //DrawFoodFirst();

            ClientSize = new Size(500, 500);

            // Timer steuert das Game (Geschwindigkeit von der Snake)
            
            timer.Tick += Game;
            timer.Interval = 100;
            timer.Start();

            
            

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g = CreateGraphics();

            g.DrawString(cl.ml.count.ToString(), new Font("Arial", 20), Brushes.Black, new Point(450, 450));
            Pen f = new Pen(Color.Green, 5);
            //g.DrawLine(f, 0, 0, 500, 0);
            //g.DrawLine(f, 0, 0, 0, 500);
            //g.DrawLine(f, 500, 0, 500, 500);
            //g.DrawLine(f, 500, 500, 0, 500);

            DrawFoodFirst();
            DrawSnake();
        }

        public void DrawRect(int x, int y, Color color, int size = 10)
        {
            g = CreateGraphics();
            SolidBrush brush = new SolidBrush(color);
            g.FillRectangle(brush, new Rectangle(x, y, size, size));
            brush.Dispose();
            g.Dispose();
        }

        private void Game(object sender, EventArgs e)
        {
            cl.draw();
            Invalidate();
        }

        private void DrawSnake()
        {
            foreach (var a in cl.ml.snake)
            {
                DrawRect(a.X, a.Y, Color.Blue);
            }
        }

        public void DrawFoodFirst()
        {
            
            DrawRect(cl.ml.food.X, cl.ml.food.Y, Color.Orange); 
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //cl.Keyboard(e.KeyChar);

            switch (e.KeyChar)
            {
                case 'w':
                    cl.Keyboard("W");
                    break;
                case 'a':
                    cl.Keyboard("A");
                    break;
                case 's':
                    cl.Keyboard("S");
                    break;
                case 'd':
                    cl.Keyboard("D");
                    break;
            }
        }

       
    }
}
