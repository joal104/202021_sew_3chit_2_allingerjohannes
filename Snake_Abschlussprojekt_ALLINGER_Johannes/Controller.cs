﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace Snake_Abschlussprojekt_ALLINGER_Johannes
{
    class Controller
    {
        public Model ml = new Model();

        //public event EventHandler Tick;
        


        
        public void Keyboard(string key)
        {
            switch (key)
            {
                case "W":
                    ml.snakestart.X = 0;
                    ml.snakestart.Y = -ml.snakeStep;
                    ml.test = Richtung.N;
                    break;
                case "A":
                    ml.snakestart.X = -ml.snakeStep;
                    ml.snakestart.Y = 0;
                    ml.test = Richtung.W;
                    break;
                case "S":
                    ml.snakestart.X = 0;
                    ml.snakestart.Y = ml.snakeStep;
                    ml.test = Richtung.S;
                    break;
                case "D":
                    ml.snakestart.X = ml.snakeStep;
                    ml.snakestart.Y = 0;
                    ml.test = Richtung.O;
                    break;
            }
        }

        public bool CollisionFood()
        {
            if (ml.snake[0].X == ml.food.X && ml.snake[0].Y == ml.food.Y)
            {
                // Add new element in the snake
                ml.snake.Add(new Point(ml.food.X, ml.food.Y));

                ml.count += 1;

                // Generate a new food position

                GenerateFood();
            }

            return true;
        }

        public void GenerateFood()
        {
            ml.food.X = 10 * ml.r.Next(0, ml.fieldHeight / 10 - 5);
            ml.food.Y = 10 * ml.r.Next(0, ml.fieldHeight / 10 - 5);
        }

        public void draw()
        {
            Point newHeadPosition = new Point(
                ml.snake[0].X + ml.snakestart.X,
                ml.snake[0].Y + ml.snakestart.Y
            );

            // Insert new position in the beginning of the snake list
            ml.snake.Insert(0, newHeadPosition);

            // Remove the last element
            ml.snake.RemoveAt(ml.snake.Count - 1);

            CollisionFood();

            Grenze();
        }

        /*public void Move()
        {
            switch(ml.test)
            {
                case Richtung.N:
                    ml.snakestart.Y += 10;
                    break;

                case Richtung.O:
                    ml.snakestart.X += 10;
                    break;

                case Richtung.S:
                    ml.snakestart.Y -= 10;
                    break;

                case Richtung.W:
                    ml.snakestart.X -= 10;
                    break;
            }
        }*/

        public void Grenze()
        {
            if(ml.snake[ml.snake.Count-1].X == 510 || ml.snake[ml.snake.Count - 1].Y == 510)
            {
                MessageBox.Show("Verloren");
            }
            else if(ml.snake[ml.snake.Count - 1].X == -10 || ml.snake[ml.snake.Count - 1].Y == -10)
            {
                MessageBox.Show("Verloren");
            }


        }
    }
}
